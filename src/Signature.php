<?php

namespace Nerones\Pdf\DigitalSignature;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Represents a signature
 */
class Signature
{
    protected $extraData = [];

    public function getCommonName()
    {
        return $this->commonName;
    }

    public function setCommonName(string $commonName)
    {
        $this->commonName = $commonName;
    }

    public function getCerSerial()
    {
        return $this->certSerial;
    }

    public function setCertSerial(string $certSerial)
    {
        $this->certSerial = $certSerial;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
    }

    public function getSigningTime()
    {
        return $this->signingTime;
    }

    public function setSigningTime(Carbon $signingTime)
    {
        $this->signingTime = $signingTime;
    }

    public function getCertRevocationDate()
    {
        if (empty($this->certRevocationDate)) {
            return null;
        }

        return $this->certRevocationDate;
    }

    public function setCertRevocationDate(Carbon $revocationDate)
    {
        $this->certRevocationDate = $revocationDate;
    }

    public function getCertNotBefore()
    {
        return $this->certNotBefore;
    }

    public function setCertNotBefore(Carbon $signingTime)
    {
        $this->certNotBefore = $signingTime;
    }

    public function getCertNotAfter()
    {
        return $this->certNotAfter;
    }

    public function setCertNotAfter(Carbon $signingTime)
    {
        $this->certNotAfter = $signingTime;
    }

    public function setSignature(int $certStatus, string $raw)
    {
        $this->signatureData = new SignatureStatus($certStatus, $raw);
    }

    public function getSignature()
    {
        if (isset($this->signatureData)) {
            return $this->signatureData;
        }
        return SignatureStatus::makeUnknown();
    }

    public function setCertificate(int $certStatus, string $raw)
    {
        $this->certificateData = new CertificateStatus($certStatus, $raw);
    }

    public function getCertificate()
    {
        if (isset($this->certificateData)) {
            return $this->certificateData;
        }
        return CertificateStatus::makeUnknown();
    }

    public function setCertIssuer(string $certIssuer)
    {
        $this->certIssuer = $certIssuer;
    }

    public function getCertIssuer() : ?string
    {
        if (empty($this->certIssuer)) {
            return null;
        }

        return $this->certIssuer;
    }

    public function setExtraData(array $extra) : void
    {
        $this->extraData += $extra;
    }

    public function getExtraData() : array
    {
        return $this->extraData;
    }

    public function valid()
    {
        if ($this->getCertificate()->isTrusted() && $this->getSignature()->isValid()) {
            return true;
        }

        if (!$this->getSignature()->isValid()) {
            // If the signature is invalid, then theres no need for more checks
            return false;
        }

        $isRevoked = $this->getCertificate()->isRevoked();
        $isExpired = $this->getCertificate()->isExpired();

        if (!$isExpired && !$isRevoked) {
            // Only check for this conditions, other invalid states
            // doesn't need extra checks
            return false;
        }

        if ($isRevoked) {
            $signedAfterRevocation = $this->getSigningTime()->greaterThan($this->getCertRevocationDate());
            if ($signedAfterRevocation) {
                return false;
            }
        }

        // Always check this, it can't be valid if the signing time it's outside
        // the valid period.
        if (! ($this->getSigningTime()->greaterThan($this->getCertNotBefore()) &&
            $this->getSigningTime()->lessThan($this->getCertNotAfter())) ) {
            // Signed outside of the valid range of dates
            return false;
        }

        return true;
    }

    public function toArray()
    {
        $data = [
            'common_name' => $this->getCommonName(),
            'full_name' => $this->getFullName(),
            'signing_time' => $this->getSigningTime()->toW3cString(),
            'cert_not_before' => $this->getCertNotBefore()->toW3cString(),
            'cert_not_after' => $this->getCertNotAfter()->toW3cString(),
            'cert_serial' => $this->getCerSerial(),
            'signature_validation' => $this->getSignature()->isValid(),
            'signature_data' => $this->getSignature()->getReadableStatus(),
            'signature_status_id' => $this->getSignature()->getStatus(),
            'certificate_validation' => $this->getCertificate()->isValid(),
            'certificate_data' => $this->getCertificate()->getReadableStatus(),
            'certificate_status_id' => $this->getCertificate()->getStatus(),
        ];

        if ($this->getCertRevocationDate() !== null) {
            $data['cert_revocation_date'] = $this->getCertRevocationDate()->toW3cString();
        }

        if ($this->getCertIssuer()) {
            $data['cert_issuer'] = $this->getCertIssuer();
        }

        if (!empty($this->getExtraData())) {
            $data['extra'] = $this->getExtraData();
        }

        return $data;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
