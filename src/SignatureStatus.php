<?php

namespace Nerones\Pdf\DigitalSignature;

/**
 *
 */
class SignatureStatus
{
    const VALID = 1;
    const INVALID = 2;
    const DIGEST_MISMATCH = 3;
    const DECODING_ERROR = 4;
    const NOT_VERIFIED = 5;
    const UNKNOWN= 6;

    const READABLE_STATUSES = [
        self::VALID => "La firma es valida.",
        self::INVALID => "La firma es invalida.",
        self::DIGEST_MISMATCH => "El documento fue alterado.",
        self::DECODING_ERROR => "El documento no esta firmado o tiene datos corruptos.",
        self::NOT_VERIFIED => "La firma no fue verificada aun.",
        self::UNKNOWN => "Error de validación desconocido.",
    ];

    /**
     *
     */
    public function __construct(int $status, string $raw)
    {
        if (!array_key_exists($status, self::READABLE_STATUSES)) {
            throw new UnknownStatusException("The declared status is unknown: $status");
        }
        $this->currentStatus = $status;
        $this->originalData = $raw;
    }

    public function getStatus()
    {
        return $this->currentStatus;
    }

    public static function makeUnknown()
    {
        return new self(self::UNKNOWN, 'Empty response');
    }

    public function getReadableStatus()
    {
        return self::READABLE_STATUSES[$this->currentStatus];
    }

    public function isValid()
    {
        return $this->currentStatus === self::VALID;
    }
}
