<?php

namespace Nerones\Pdf\DigitalSignature;

use Nerones\Pdf\DigitalSignature\Driver\Driver;

class Validator
{
    private $driver = null;

    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Validate a pdf file
     *
     * @return Result a object representing the status of the signature
     */
    public function validate(string $file)
    {
        return $this->getDriver()->check($file);
    }

    /**
     * Gets the value of driver
     *
     * @return Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }
}
