<?php

namespace Nerones\Pdf\DigitalSignature\Driver\Poppler;

class Executor
{
    /**
     * Runs the pdfsig executable on the host machine.
     *
     * Return an array with the lines of the output
     * in each item.
     */
    public function run(string $file) : array
    {
        $output = [];
        exec("pdfsig {$file}", $output, $exitCode);


        return [$exitCode, $output];
    }
}
