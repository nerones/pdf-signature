<?php

namespace Nerones\Pdf\DigitalSignature\Driver;

use Exception;
use Carbon\Carbon;
use Nerones\Pdf\DigitalSignature\Signature;
use Nerones\Pdf\DigitalSignature\Result;
use Nerones\Pdf\DigitalSignature\SignatureStatus;
use Nerones\Pdf\DigitalSignature\CertificateStatus;

/**
 * Validates the digital signature of a pdf file using the
 * poppler utils from freedesktop
 */
class Fake implements Driver
{
    protected $isValid = true;

    public function __construct($isValid = true)
    {
        $this->isValid = $isValid;
    }

    public function check(string $file)
    {
        $signatures = $this->getSignatures();
        $result = new Result($signatures);

        return $result;
    }

    protected function getSignatures()
    {
        $signatures = [];

        foreach (range(0, rand(1, 5)) as $value) {
            $signature = new Signature();
            $signature->setCommonName('common name');
            $signature->setFullName('Full name');
            $signature->setCertNotBefore(Carbon::yesterday());
            $signature->setSigningTime(Carbon::today());
            $signature->setCertNotAfter(Carbon::tomorrow());
            $signature->setSignature(
                $this->isValid? SignatureStatus::VALID : SignatureStatus::UNKNOWN,
                'Test data'
            );
            $signature->setCertificate(
                $this->isValid? CertificateStatus::TRUSTED : CertificateStatus::UNKNOWN,
                'Test data'
            );
            $signature->setCertSerial('111111111111');

            $signatures[] = $signature;
        }

        return $signatures;
    }
}
