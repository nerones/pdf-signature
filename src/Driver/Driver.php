<?php

namespace Nerones\Pdf\DigitalSignature\Driver;

/**
 * Provides a driver to validate a pdf
 *
 * @package default
 * @author Nelson Efrain A. Cruz
 */
interface Driver
{
    public function check(string $file);
}
