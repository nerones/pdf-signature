<?php

namespace Nerones\Pdf\DigitalSignature\Driver;

use Exception;
use Carbon\Carbon;
use Nerones\Pdf\DigitalSignature\SignatureStatus;
use Nerones\Pdf\DigitalSignature\CertificateStatus;
use Nerones\Pdf\DigitalSignature\Signature;
use Nerones\Pdf\DigitalSignature\Result;
use Nerones\Pdf\DigitalSignature\Driver\Poppler\Executor;

/**
* Validates the digital signature of a pdf file using the
* poppler utils from freedesktop
 */
class Poppler implements Driver
{
    const SIGNATURE_NOT_FOUND = "does not contain any signatures";
    const SIGNATURE_FOUND_HEADER = "Signature #";
    const SIGNATURE_ITEM_COMMON_NAME = "Signer Certificate Common Name:";
    const SIGNATURE_ITEM_FULL_NAME = "Signer full Distinguished Name:";
    const SIGNATURE_ITEM_SIGNING_TIME = "Signing Time:";
    const SIGNATURE_ITEM_SIGNATURE_VALIDATION = "Signature Validation:";
    const SIGNATURE_ITEM_CERTIFICATE_VALIDATION = "Certificate Validation:";
    const SIGNATURE_ITEM_CERT_NOT_BEFORE = "Signer Cert not before:";
    const SIGNATURE_ITEM_CERT_NOT_AFTER = "Signer Cert not after:";
    const SIGNATURE_ITEM_CERT_SERIAL = "Signer Cert serial number:";

    /**
     * The process runner
     */
    private $executor = null;

    public function __construct(Executor $executor = null)
    {
        if ($executor == null) {
            $executor = new Executor();
        }

        $this->executor = $executor;
    }

    public function check(string $file)
    {
        list($exitCode, $output) = $this->executor->run($file);

        if (count($output) == 1) {
            if (strpos($output[0], self::SIGNATURE_NOT_FOUND) !== false) {
                return new Result([]); //Empty result
            }
            throw new Exception("No se pudo validar la salida = " . implode(' | ', $output));
        }

        if ($exitCode !== 0) {
            //Cuando puede validar el exit code es 0
            throw new Exception("La salida del comando fue distinta de 0 y hay mas de una linea de salida");
        }

        array_shift($output);
        $signatures = $this->getSignatures($output);
        $result = new Result($signatures);

        return $result;
    }

    protected function getSignatures(array $rawSignatures)
    {
        $signatures = [];
        $counter = -1;
        foreach ($rawSignatures as $output) {
            if (strpos($output, self::SIGNATURE_FOUND_HEADER) !== false) {
                $counter++;
                $signatures[$counter] = new Signature();
                continue;
            }

            if (strpos($output, " - ") !== 0) {
                $this->proccessLine($output, $signatures[$counter]);
            }
        }

        return $signatures;
    }

    protected function proccessLine(string $line, Signature &$signature)
    {
        $charsToClean = [' ', '-', ' '];
        foreach ($charsToClean as $char) {
            $line = ltrim($line, $char);
        }

        if (strpos($line, self::SIGNATURE_ITEM_COMMON_NAME) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_COMMON_NAME, '', $line));
            if ($line === '(null)') {
                $line = 'No se pudieron leer datos';
            }
            $signature->setCommonName($line);
            return ['common_name', $line];
        }

        if (strpos($line, self::SIGNATURE_ITEM_CERT_SERIAL) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_CERT_SERIAL, '', $line));
            if ($line === '(null)') {
                $line = 'No se pudieron leer datos';
            }
            $signature->setCertSerial($line);
            return ['cert_serial', $line];
        }

        if (strpos($line, self::SIGNATURE_ITEM_FULL_NAME) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_FULL_NAME, '', $line));
            if ($line === '(null)') {
                $line = 'No se pudieron leer datos';
            }

            $signature->setFullName($line);
            return ['full_name', $line];
        }

        if (strpos($line, self::SIGNATURE_ITEM_SIGNING_TIME) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_SIGNING_TIME, '', $line));

            $signature->setSigningTime($this->readDate($line));
            return ['signing_time', $line];
        }

        if (strpos($line, self::SIGNATURE_ITEM_CERT_NOT_BEFORE) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_CERT_NOT_BEFORE, '', $line));

            $signature->setCertNotBefore($this->readDate($line));
            return ['cert_not_before', $line];
        }

        if (strpos($line, self::SIGNATURE_ITEM_CERT_NOT_AFTER) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_CERT_NOT_AFTER, '', $line));

            $signature->setCertNotAfter($this->readDate($line));
            return ['cert_not_after', $line];
        }

        if (strpos($line, self::SIGNATURE_ITEM_SIGNATURE_VALIDATION) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_SIGNATURE_VALIDATION, '', $line));

            $signature->setSignature($this->readSignatureStatus($line), $line);
            return ['signature_validation', $line];
        }

        if (strpos($line, self::SIGNATURE_ITEM_CERTIFICATE_VALIDATION) !== false) {
            $line = ltrim(str_replace(self::SIGNATURE_ITEM_CERTIFICATE_VALIDATION, '', $line));

            $signature->setCertificate($this->readCertificate($line), $line);
            return ['certificate_validation', $line];
        }

        return null;
    }

    private function readSignatureStatus($line)
    {
        $signatureData = [
            SignatureStatus::VALID           => "Signature is Valid.",
            SignatureStatus::INVALID         => "Signature is Invalid.",
            SignatureStatus::DIGEST_MISMATCH => "Digest Mismatch.",
            SignatureStatus::DECODING_ERROR  => "Document isn't signed or corrupted data.",
            SignatureStatus::NOT_VERIFIED    => "Signature has not yet been verified.",
            SignatureStatus::UNKNOWN         => "Unknown Validation Failure.",
        ];

        $index = array_search($line, $signatureData);

        // Index is false if it's not founded
        return $index ?: SignatureStatus::UNKNOWN;
    }

    private function readCertificate($line)
    {
        $certData = [
            CertificateStatus::TRUSTED          =>  "Certificate is Trusted.",
            CertificateStatus::UNTRUSTED_ISSUER =>  "Certificate issuer isn't Trusted.",
            CertificateStatus::UNKNOWN_ISSUER   =>  "Certificate issuer is unknown.",
            CertificateStatus::REVOKED          =>  "Certificate has been Revoked.",
            CertificateStatus::EXPIRED          =>  "Certificate has Expired",
            CertificateStatus::NOT_VERIFIED     =>  "Certificate has not yet been verified.",
            CertificateStatus::UNKNOWN          =>  "Unknown issue with Certificate or corrupted data.",
        ];

        $index = array_search($line, $certData);

        return $index ?: CertificateStatus::UNKNOWN;
    }

    private function readDate(string $time)
    {
        try {
            return Carbon::createFromFormat("M d Y H:i:s", $time);
        } catch (Exception $e) {
            throw new \Exception('No se puede parsear la fecha del pdf');
        }
    }
}
