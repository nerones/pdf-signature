<?php

namespace Nerones\Pdf\DigitalSignature;

/**
 * Handles the status of a certificate, to make better controls
 * of the validity of a signed document.
 */
class CertificateStatus
{

    const TRUSTED= 1;
    const UNTRUSTED_ISSUER= 2;
    const UNKNOWN_ISSUER= 3;
    const REVOKED= 4;
    const EXPIRED= 5;
    const NOT_VERIFIED= 6;
    const UNKNOWN= 7;
    /**
     * The certificate chain has issues, and the pdf was signed after revocation or
     * after expiration of some cert in the chain, not the leaf.
     */
    const CERTIFICATE_CHAIN_UNACCEPTABLE = 8;
    /**
     * The certificate chain has issues, but the signature was made before the signature
     * has been invalidated
     */
    const CERTIFICATE_CHAIN_ACCEPTABLE = 9;
    const READABLE_STATUSES = [
        self::TRUSTED => "El certificado es confiable.",
        self::UNTRUSTED_ISSUER => "Emisor de certificado no confiable.",
        self::UNKNOWN_ISSUER => "Emisor de certificado desconocido.",
        self::REVOKED => "El certificado fue revocado.",
        self::EXPIRED => "El certificado expiro.",
        self::NOT_VERIFIED => "El certificado todavía no fue verificado.",
        self::UNKNOWN => "Error en el certificado o datos corruptos.",
        self::CERTIFICATE_CHAIN_ACCEPTABLE => "La cadena de certificados tiene problemas pero no afecta la validez del documento",
        self::CERTIFICATE_CHAIN_UNACCEPTABLE => "La cadena de certificados tiene problemas e invalida al documento"
    ];

    protected $currentStatus = self::UNKNOWN;
    protected $originalData = null;


    /**
     *
     */
    public function __construct(int $status, string $raw)
    {
        if (!array_key_exists($status, self::READABLE_STATUSES)) {
            throw new UnknownStatusException("The declared status is unknown: $status");
        }
        $this->currentStatus = $status;
        $this->originalData = $raw;
    }

    public static function makeUnknown()
    {
        return new self(self::UNKNOWN, 'Empty response');
    }

    public function getReadableStatus()
    {
        return self::READABLE_STATUSES[$this->currentStatus];
    }

    public function getStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Alias for self::isTrusted
     * If a certificate it's trusted then we can say that it's valid
     */
    public function isValid()
    {
        return $this->isTrusted();
    }

    public function isTrusted()
    {
        return $this->inStatus(self::TRUSTED) || $this->inStatus(self::CERTIFICATE_CHAIN_ACCEPTABLE);
    }

    public function isRevoked()
    {
        return $this->inStatus(self::REVOKED);
    }

    public function isExpired()
    {
        return $this->inStatus(self::EXPIRED);
    }

    protected function inStatus(int $status)
    {
        return $this->currentStatus === $status;
    }
}
