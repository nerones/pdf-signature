<?php

namespace Nerones\Pdf\DigitalSignature;

/**
 * Represents the result of a validation
 * It haves one or more signatures
 *
 */
class Result
{
    public function __construct(array $signatures)
    {
        foreach ($signatures as $signature) {
            if (!is_a($signature, Signature::class)) {
                throw new \Exception("The signatures must be ".Signature::class);
            }
        }

        $this->signatures = $signatures;
    }

    public function signed()
    {
        return $this->count() >= 1;
    }

    public function valid()
    {
        if (!$this->signed()) {
            return false;
        }
        foreach ($this->getSignatures() as $signature) {
            if (!$signature->valid()) {
                return false;
            }
        }

        return true;
    }

    public function count()
    {
        return count($this->getSignatures());
    }

    public function getSignatures()
    {
        return $this->signatures;
    }

    public function getSignature(int $position)
    {
        return $this->signatures[$position];
    }

    public function toArray()
    {
        $signatures = [];
        foreach ($this->getSignatures() as $signature) {
            $signatures[] = $signature->toArray();
        }
        return [
            'signatures' => $signatures
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
