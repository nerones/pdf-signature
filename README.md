Extracts digital signatures from pdf files with external tools. Right now uses pdfsig

[![pipeline status](https://gitlab.com/nerones/pdf-signature/badges/master/pipeline.svg)](https://gitlab.com/nerones/pdf-signature/commits/master)
[![coverage report](https://gitlab.com/nerones/pdf-signature/badges/master/coverage.svg)](https://gitlab.com/nerones/pdf-signature/commits/master)
