<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Nerones\Pdf\DigitalSignature\Result;
use Nerones\Pdf\DigitalSignature\Signature;

final class ResultTest extends TestCase
{
    /**
     * @test
     *
     */
    public function onlyCreateObjectWithProperData() : void
    {
         $this->expectException(\Exception::class);

         new Result([new \stdClass]);
    }
    /**
     * @test
     *
     */
    public function canCreateEmptyResult() : void
    {
         $result = new Result([]);

         $this->assertFalse($result->signed());
         $this->assertFalse($result->valid());
         $this->assertEquals(0, $result->count());
    }

    /**
     * @test
     *
     */
    public function checkCountOfSigntarues() : void
    {
        $result = new Result([new Signature,]);

        $this->assertEquals(1, $result->count());
    }

    /**
     * @test
     *
     */
    public function checSignatureGetter() : void
    {
        $signatures = [new Signature];

        $result = new Result($signatures);

        $this->assertEquals($signatures, $result->getSignatures());
        $this->assertEquals($signatures[0], $result->getSignature(0));
    }

    /**
     * @test
     *
     */
    public function checkIfSigned() : void
    {
        $resultSigned = new Result([new Signature]);

        $this->assertTrue($resultSigned->signed());
    }

    /**
     * @test
     *
     */
    public function shouldBeInvalidResult() : void
    {
        $result = new Result([]);

        $this->assertFalse($result->valid());
    }

    /**
     * @test
     *
     */
    public function shoulBeInvalidResultWithOneInvalidSignature() : void
    {
        $sign1 = $this->createMock(Signature::class);
        $sign1->method('valid')
             ->willReturn(true);
        $sign2 = $this->createMock(Signature::class);
        $sign2->method('valid')
             ->willReturn(false);

        $signatures = [$sign1, $sign2];
        $result = new Result($signatures);

        $this->assertFalse($result->valid());
    }

    /**
     * @test
     *
     */
    public function shoulBeValidResult() : void
    {
        $sign1 = $this->createMock(Signature::class);
        $sign1->method('valid')
             ->willReturn(true);
        $sign2 = $this->createMock(Signature::class);
        $sign2->method('valid')
             ->willReturn(true);

        $signatures = [$sign1, $sign2];
        $result = new Result($signatures);

        $this->assertTrue($result->valid());
    }

    /**
     * @test
     *
     */
    public function checkJsonConvertion() : void
    {
        $output = ['signatures' => []];

        $emptyResult = new Result([]);

        $this->assertJsonStringEqualsJsonString(
            json_encode($output),
            $emptyResult->toJson()
        );

        $sign1 = $this->createMock(Signature::class);
        $sign1->method('toArray')
            ->willReturn(['somefield' => 'somevalue']);

        $output = ['signatures' => [['somefield' => 'somevalue']]];
        $result = new Result([$sign1]);

        $this->assertJsonStringEqualsJsonString(
            json_encode($output),
            $result->toJson()
        );
    }
}
