<?php

namespace Tests\Concerns;

use Nerones\Pdf\DigitalSignature\Driver\Poppler\Executor;

trait EmulatesPopplerExecutor
{
    public function createsCorrectExecutor()
    {
        $stub = $this->createMock(Executor::class);

        // Configure the stub.
        $stub->method('run')
            ->willReturn(
                [
                    0,
                    [
                        'Digital Signature Info of: tests/assets/1.pdf',
                        'Signature #1:',
                        '  - Signer Certificate Common Name: APARICIO Marina Fernanda',
                        '  - Signer full Distinguished Name: CN=APARICIO Marina Fernanda,C=AR,serialNumber=CUIL 27252187869',
                        '  - Signer Cert not before: Mar 20 2017 14:51:30',
                        '  - Signer Cert not after: Mar 20 2019 14:51:30',
                        '  - Signer Cert serial number: 1944C15B000000052535',
                        '  - Signing Time: Dec 07 2017 16:09:28',
                        '  - Signing Hash Algorithm: SHA1',
                        '  - Signature Type: adbe.pkcs7.detached',
                        '  - Signed Ranges: [0 - 27008], [97938 - 111959]',
                        '  - Total document signed',
                        '  - Signature Validation: Signature is Valid.',
                        '  - Certificate Validation: Certificate has been Revoked.',
                    ]
                ]
            );
        return $stub;
    }

    public function createsCorruptedSignature()
    {
        $stub = $this->createMock(Executor::class);

        // Configure the stub.
        $stub->method('run')
            ->willReturn(
                [
                    0,
                    [
                        'Digital Signature Info of: tests/assets/1.pdf',
                        'Signature #1:',
                        '  - Signer Certificate Common Name: (null)',
                        '  - Signer full Distinguished Name: (null)',
                        '  - Signer Cert serial number: (null)',
                    ]
                ]
            );
        return $stub;
    }

    public function createsCorruptedDateSignature()
    {
        $stub = $this->createMock(Executor::class);

        // Configure the stub.
        $stub->method('run')
            ->willReturn(
                [
                    0,
                    [
                        'Digital Signature Info of: tests/assets/1.pdf',
                        'Signature #1:',
                        '  - Signer Cert not before: 2017 14:51:30',
                    ]
                ]
            );
        return $stub;
    }

    public function createsFailedExecution()
    {
        $stub = $this->createMock(Executor::class);

        // Configure the stub.
        $stub->method('run')
            ->willReturn(
                [
                    3,
                    [
                        'Some Error',
                    ]
                ]
            );
        return $stub;
    }
}
