<?php

namespace Tests\Driver;

use PHPUnit\Framework\TestCase;
use Nerones\Pdf\DigitalSignature\Driver\Poppler;
use Nerones\Pdf\DigitalSignature\Result;
use Tests\Concerns\EmulatesPopplerExecutor;

final class PopplerTest extends TestCase
{
    use EmulatesPopplerExecutor;

    protected $pdfPath = __DIR__.'/../assets/1.pdf';
    protected $invalidPdf = __DIR__.'/../assets/notA.pdf';
    protected $inexistentPdf = __DIR__.'/../assets/notInDisk.pdf';
    protected $unsignedPdfPath = __DIR__.'/../assets/unsigned.pdf';

    public function setUp()
    {
        $this->poppler = new Poppler;
    }

    /**
     * @test
     *
     */
    public function validateInvalidFile()
    {
        $this->expectException(\Exception::class);
        $this->poppler->check($this->invalidPdf);
    }

    /**
     * @test
     *
     */
    public function validateInexistentFile()
    {
        $this->expectException(\Exception::class);
        $this->poppler->check($this->inexistentPdf);
    }

    /**
     * @test
     *
     */
    public function validateUnsignedPdf()
    {
        $this->assertInstanceOf(
            Result::class,
            $this->poppler->check($this->unsignedPdfPath)
        );
    }

    /**
     * test
     * disabled for now
     */
    public function check()
    {
        $this->assertInstanceOf(
            Result::class,
            $this->poppler->check($this->pdfPath)
        );
    }

    /**
     * @test
     *
     */
    public function checkCustomPoppler()
    {
        $poppler = new Poppler($this->createsCorrectExecutor());
        $this->assertInstanceOf(
            Result::class,
            $poppler->check($this->pdfPath)
        );
    }

    /**
     * @test
     *
     */
    public function checkCorruptedSignature()
    {
        $poppler = new Poppler($this->createsCorruptedSignature());
        $this->assertInstanceOf(
            Result::class,
            $poppler->check($this->pdfPath)
        );
    }

    /**
     * @test
     *
     */
    public function checkFailedExcution()
    {
        $poppler = new Poppler($this->createsFailedExecution());
        $this->expectException(\Exception::class);
        $poppler->check($this->pdfPath);
    }

    /**
     * @test
     *
     */
    public function checkFailedParsingOfDate()
    {
        $poppler = new Poppler($this->createsCorruptedDateSignature());
        $this->expectException(\Exception::class);
        var_dump($poppler->check($this->pdfPath));
    }
}
