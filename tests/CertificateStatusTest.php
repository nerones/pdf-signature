<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Nerones\Pdf\DigitalSignature\CertificateStatus;
use Nerones\Pdf\DigitalSignature\UnknownStatusException;

final class CertificateStatusTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateWithUnknownStatus(): void
    {
        $status = CertificateStatus::makeUnknown();
        $this->assertEquals(
            CertificateStatus::UNKNOWN,
            $status->getStatus()
        );
    }

    /**
     * @test
     *
     */
    public function canCreteValidStatus() : void
    {
        $status = new CertificateStatus(CertificateStatus::TRUSTED, 'not implemented');

        $this->assertEquals(
            CertificateStatus::TRUSTED,
            $status->getStatus()
        );
        $this->assertTrue($status->isValid());
        $this->assertTrue($status->isTrusted());
    }

    public function getInvalidStatus()
    {
        return [
            [CertificateStatus::UNTRUSTED_ISSUER],
            [CertificateStatus::UNKNOWN_ISSUER],
            [CertificateStatus::REVOKED],
            [CertificateStatus::EXPIRED],
            [CertificateStatus::NOT_VERIFIED],
            [CertificateStatus::UNKNOWN],
        ];
    }

    /**
     * @test
     * @dataProvider getInvalidStatus
     *
     */
    public function createsInvalidStatus($statusCode) : void
    {
        $status = new CertificateStatus($statusCode, 'not implemented');
        $this->assertFalse($status->isValid());
    }

    /**
     * @test
     *
     */
    public function createWithANotDefinedStatus() : void
    {
        $this->expectException(UnknownStatusException::class);

        new CertificateStatus(1000, 'not implemented');
    }

    /**
     * @test
     *
     */
    public function canGetReadableStatus() : void
    {
        $status = new CertificateStatus(CertificateStatus::TRUSTED, 'not implemented');

        $this->assertEquals(
            CertificateStatus::READABLE_STATUSES[CertificateStatus::TRUSTED],
            $status->getReadableStatus()
        );
    }

    public function statusAndCheckers()
    {
        return [
            [CertificateStatus::TRUSTED, 'isTrusted'],
            [CertificateStatus::EXPIRED, 'isExpired'],
            [CertificateStatus::REVOKED, 'isRevoked'],
        ];
    }

    /**
     * @test
     * @dataProvider statusAndCheckers
     *
     */
    public function checkRevokedStatus($status, $checker) : void
    {
        $status = new CertificateStatus($status, 'not implemented');
        $this->assertTrue($status->$checker());
    }
}
