<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Nerones\Pdf\DigitalSignature\SignatureStatus;
use Nerones\Pdf\DigitalSignature\UnknownStatusException;

final class SignatureStatusTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateWithUnknownStatus(): void
    {
        $status = SignatureStatus::makeUnknown();
        $this->assertEquals(
            SignatureStatus::UNKNOWN,
            $status->getStatus()
        );
    }

    /**
     * @test
     *
     */
    public function canCreteValidStatus() : void
    {
        $status = new SignatureStatus(SignatureStatus::VALID, 'not implemented');

        $this->assertEquals(
            SignatureStatus::VALID,
            $status->getStatus()
        );
        $this->assertTrue($status->isValid());
    }

    public function getInvalidStatus()
    {
        return [
            [SignatureStatus::INVALID],
            [SignatureStatus::DIGEST_MISMATCH],
            [SignatureStatus::DECODING_ERROR],
            [SignatureStatus::NOT_VERIFIED],
            [SignatureStatus::UNKNOWN],
        ];
    }

    /**
     * @test
     * @dataProvider getInvalidStatus
     *
     */
    public function createsInvalidStatus($statusCode) : void
    {
        $status = new SignatureStatus($statusCode, 'not implemented');
        $this->assertFalse($status->isValid());
    }

    /**
     * @test
     *
     */
    public function createWithANotDefinedStatus() : void
    {
        $this->expectException(UnknownStatusException::class);

        new SignatureStatus(1000, 'not implemented');
    }

    /**
     * @test
     *
     */
    public function canGetReadableStatus() : void
    {
        $status = new SignatureStatus(SignatureStatus::VALID, 'not implemented');

        $this->assertEquals(
            SignatureStatus::READABLE_STATUSES[SignatureStatus::VALID],
            $status->getReadableStatus()
        );
    }
}
