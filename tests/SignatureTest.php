<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Nerones\Pdf\DigitalSignature\Signature;
use Nerones\Pdf\DigitalSignature\CertificateStatus;
use Nerones\Pdf\DigitalSignature\SignatureStatus;
use Carbon\Carbon;

final class SignatureTest extends TestCase
{
    public function setUp()
    {
        $this->signature = new Signature;
    }

    public function getterAndSetters()
    {
        return [
            ['setCommonName', 'getCommonName', 'Test Name'],
            ['setCertSerial', 'getCerSerial', 'ASDASDASDASDDSFSREF'],
            ['setFullName', 'getFullName', 'Complete Name'],
            ['setSigningTime', 'getSigningTime', Carbon::now()],
            ['setCertRevocationDate', 'getCertRevocationDate', Carbon::now()],
            ['setCertNotBefore', 'getCertNotBefore', Carbon::now()],
            ['setCertNotAfter', 'getCertNotAfter', Carbon::now()],
        ];
    }

    /**
     * @test
     * @dataProvider getterAndSetters
     *
     */
    public function checkSettersAndGetters($set, $get, $value) : void
    {
        $this->signature->$set($value);
        $this->assertEquals(
            $value,
            $this->signature->$get()
        );
    }

    /**
     * @test
     *
     */
    public function checkCertificateCreation() : void
    {
        $this->signature->setCertificate(CertificateStatus::TRUSTED, 'ignored');

        $this->assertInstanceOf(
            CertificateStatus::class,
            $this->signature->getCertificate()
        );
    }

    /**
     * @test
     *
     */
    public function checkStatusCreation() : void
    {
        $this->signature->setSignature(SignatureStatus::VALID, 'ignored');

        $this->assertInstanceOf(
            SignatureStatus::class,
            $this->signature->getSignature()
        );
    }

    /**
     * @test
     *
     */
    public function chekSignatureStatusNotDefined() : void
    {
        $this->assertEquals(
            SignatureStatus::UNKNOWN,
            $this->signature->getSignature()->getStatus()
        );
    }

    /**
     * @test
     *
     */
    public function chekCertificateStatusNotDefined() : void
    {
        $this->assertEquals(
            CertificateStatus::UNKNOWN,
            $this->signature->getCertificate()->getStatus()
        );
    }

    /**
     * @test
     *
     */
    public function isinvalidWithInvalidSignatureAndValidCertificate() : void
    {
        $this->signature->setSignature(SignatureStatus::UNKNOWN, 'ignored');
        $this->signature->setCertificate(CertificateStatus::TRUSTED, 'ignored');

        $this->assertFalse(
            $this->signature->valid()
        );
    }

    /**
     * @test
     *
     */
    public function isinvalidWithValidSignatureAndInvalidCertificate() : void
    {
        $this->signature->setSignature(SignatureStatus::VALID, 'ignored');
        $this->signature->setCertificate(CertificateStatus::UNTRUSTED_ISSUER, 'ignored');

        $this->assertFalse(
            $this->signature->valid()
        );
    }

    /**
     * @test
     *
     */
    public function isInvalidWithRevokedCertificate() : void
    {
        $this->signature->setSignature(SignatureStatus::VALID, 'ignored');
        $this->signature->setCertificate(CertificateStatus::REVOKED, 'ignored');
        // Signed today
        $this->signature->setSigningTime(Carbon::now());
        // Revoked yesterday
        $this->signature->setCertRevocationDate(Carbon::now()->subDay());

        $this->assertFalse(
            $this->signature->valid()
        );
    }

    /**
     * @test
     *
     */
    public function isInvalidSignedOutsideOfValidPeriod() : void
    {
        $this->signature->setSignature(SignatureStatus::VALID, 'ignored');
        $this->signature->setCertificate(CertificateStatus::EXPIRED, 'ignored');
        // Signed today
        $this->signature->setSigningTime(Carbon::now());
        // Certificate was valid the past seven days
        $this->signature->setCertNotBefore(Carbon::now()->subDay(7));
        $this->signature->setCertNotAfter(Carbon::now()->subDay());

        $this->assertFalse(
            $this->signature->valid()
        );
    }

    /**
     * @test
     *
     */
    public function isValidWithValidSignatureAndValidCertificate() : void
    {
        $this->signature->setSignature(SignatureStatus::VALID, 'ignored');
        $this->signature->setCertificate(CertificateStatus::TRUSTED, 'ignored');

        $this->assertTrue(
            $this->signature->valid()
        );
    }


    /**
     * @test
     *
     */
    public function isValidEvenWithExpiredAndRevokedCert() : void
    {
        $this->signature->setSignature(SignatureStatus::VALID, 'ignored');
        $this->signature->setCertificate(CertificateStatus::REVOKED, 'ignored');

        // Signed 3 days before today, cert was valid
        $this->signature->setSigningTime(Carbon::now()->subDay(3));

        // Certificate expired yesterday
        $this->signature->setCertNotBefore(Carbon::now()->subDay(7));
        $this->signature->setCertNotAfter(Carbon::now()->subDay(1));

        // Certificate was revoked the day before yesterday
        $this->signature->setCertRevocationDate(Carbon::now()->subDay(2));

        $this->assertTrue(
            $this->signature->valid()
        );
    }

    /**
     * @test
     *
     */
    public function canConvertToJson() : void
    {
        $today = Carbon::now();
        $output = [
            'common_name' => 'a',
            'full_name' => 'b',
            'signing_time' => $today->toW3cString(),
            'cert_not_before' => $today->toW3cString(),
            'cert_not_after' => $today->toW3cString(),
            'cert_serial' => 'asdasd',
            'signature_validation' => true,
            'signature_data' => SignatureStatus::READABLE_STATUSES[SignatureStatus::VALID],
            'signature_status_id' => SignatureStatus::VALID,
            'certificate_validation' => true,
            'certificate_data' => CertificateStatus::READABLE_STATUSES[CertificateStatus::TRUSTED],
            'certificate_status_id' => CertificateStatus::TRUSTED,
        ];

        $this->signature->setCommonName($output['common_name']);
        $this->signature->setFullName($output['full_name']);
        $this->signature->setSigningTime($today);
        $this->signature->setCertNotBefore($today);
        $this->signature->setCertNotAfter($today);
        $this->signature->setCertSerial($output['cert_serial']);
        $this->signature->setSignature($output['signature_status_id'], 'ignored');
        $this->signature->setCertificate($output['certificate_status_id'], 'ignored');


        $this->assertJsonStringEqualsJsonString(
            json_encode($output),
            $this->signature->toJson()
        );

        $this->signature->setCertRevocationDate($today);
        $output['cert_revocation_date'] = $today->toW3cString();

        $this->assertJsonStringEqualsJsonString(
            json_encode($output),
            $this->signature->toJson()
        );
    }
}
