<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Nerones\Pdf\DigitalSignature\Validator;
use Nerones\Pdf\DigitalSignature\Driver\Poppler;
use Nerones\Pdf\DigitalSignature\Driver\Fake;
use Nerones\Pdf\DigitalSignature\Result;

final class ValidatorTest extends TestCase
{
    protected $pdfPath = __DIR__.'/assets/1.pdf';

    public function testCanUseFakeDriver(): void
    {
        $validator = new Validator(new Fake);
        $this->assertInstanceOf(
            Result::class,
            $validator->validate($this->pdfPath)
        );
    }

    /**
     *
     */
    public function canUsePopplerDriver(): void
    {
        $validator = new Validator(new Poppler);
        $this->assertInstanceOf(
            Result::class,
            $validator->validate($this->pdfPath)
        );
    }
}
